# Scriptjes

## Word counter
Count the words on quotes.toscrape.com and display them in a bar chart.

Example:
![](/output/word_frequencies.png)